AdjClust Adjacency Clustering for large high-dimensional datasets
-------------------------------------------------------------------------------
July 10, 2015  
Version 0.1.0

Adjacency clustering code, and parallel tSNE implementation

Dependencies
-------------------------------------------------------------------------------
R 3.0  
R packages:

  - gplots
  - hexbin
  - png
  - cytofkit

Mercurial Configuration
-------------------------------------------------------------------------------
Update Mercurial .hgignore file with:  
```
syntax: glob
.*
vignettes/*.pdf
vignettes/*.R
*.Rproj
man/*.Rd
inst/doc/*
src/*.o
src/*.so
```

Build Instructions
-------------------------------------------------------------------------------
Install build dependencies:
```R
install.packages(c("devtools", "roxygen2", "testthat", "knitr", "rmarkdown"))
```


Building with Rstudio:

-  Build -> Configure Build Tools
-  Check use devtools option
-  Check use roxygen option
-  Select configure roxygen options and check everything.
-  Build -> Build and Reload

Building from the R console:

```R
library(roxygen2)
library(devtools)
document()
install_deps()
build(vignettes=FALSE)
install()
```

Building tSNE with OpenMP support:

FOR MAC:
-  Install brew [http://brew.sh/](http://brew.sh/)

-  Install gcc4.9

```brew install /homebrew/versions/gcc49```

-  In R, run the following command:

```compileTSNE(compiler = "g++-4.9", use_omp = T)```

FOR LINUX AND WINDOWS:

-  In R, run the following command:


```compileTSNE(use_omp=T)```
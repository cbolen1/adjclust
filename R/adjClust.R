# adjacencyClustering documentation 
# 
# @author     Christopher R Bolen
# @copyright  Copyright 2015 Davis Lab, Stanford University. All rights reserved
# @license    Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported
# @version    0.1.0
# @date       2015.06.23


#' AdjacencyClustering
#' 
#' Methods for running tSNE and Adjacency Clustering 
#' 
#' @name        AdjacencyClustering
#' @docType     package
#' 
#' @import gplots
#' @import hexbin
#' @import parallel
#' @import png
#' @import cytofkit
#' @useDynLib adjClust
NULL